/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ACER
 */
public class Algorithm8 {

    public static void main(String[] args) {
        int[] A = {1, 52, 5, 4, 7, 9, 8, 46, 5, 72, 6, 2, 944, 8, 5, 83, 6, 29, 51, 7};
        int[] reverseA = new int[A.length];

        int n = A.length - 1;

        for (int i : A) {
            reverseA[n] = i;
            n--;
        }
        for (int i : A) {
            System.out.print(i + " ");
        }
        System.out.println();
        for (int i : reverseA) {
            System.out.print(i + " ");
        }
    }
}
